
# BeneAPI public documentation

This document describes BeneAPI, a RESTful API that allows 3rd parties access Benemen systems to perform various functions. BeneAPI serves data mapped as resources corresponding to URI paths. Client applications make HTTP requests to the API and handle the response. Request and response data can be serialized to either JSON or XML formats. The API is not dependent on any specific language or technology, so it can be used by any application capable of making HTTP requests and parsing the responses.

Full documentation can be found at https://doc.beneservices.com/beneapi/
